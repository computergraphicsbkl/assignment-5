﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Text.RegularExpressions;

namespace LSystems
{
    class LSystem
    {
        public List<Rule> Rules;
        public string Axiom;
        public double Angle;
        public double LineLength;
        public byte Iterations;
        public Point StartPoint;
        public string GrowString;
        public char Side;


        public LSystem(string text)
        {
            string[] lines = text.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
            Rules = new List<Rule>();
            foreach (string line in lines)
                Rules.Add(new Rule(line));
        }

		public List<Tuple<PointF, PointF>> GetLines()
		{
			Grow();
			CurrentPoint currentTurtle = new CurrentPoint();
			currentTurtle.Position = StartPoint;
			currentTurtle.CurrentAngle = GetSideAngle();
			currentTurtle.LineLength = LineLength;

			CurrentPoint nextTurtle = new CurrentPoint();
			Stack<CurrentPoint> pointStack = new Stack<CurrentPoint>();
			List<Tuple<PointF, PointF>> lineList = new List<Tuple<PointF, PointF>>();

			for (int i = 0; i < GrowString.Length; i++)
			{
				switch (GrowString[i])
				{
					case 'F':
						{
							nextTurtle = GetNextPoint(currentTurtle);
							lineList.Add(new Tuple<PointF, PointF>(GetPoint(currentTurtle), GetPoint(nextTurtle)));
							currentTurtle = (CurrentPoint)nextTurtle.Clone();
							break;
						}
					case '+':
						{
							currentTurtle.CurrentAngle += Angle;
							break;
						}
					case '-':
						{
							currentTurtle.CurrentAngle -= Angle;
							break;
						}
					case '[':
						{
							pointStack.Push((CurrentPoint)currentTurtle.Clone());
							break;
						}
					case ']':
						{
							currentTurtle = pointStack.Pop();
							break;
						}
				}
			}
			return lineList;
		}


        public List<Tuple<PointF, PointF, int, Color>> GetTreeLines()
        {
            Grow();
            CurrentPoint currentTurtle = new CurrentPoint();
            currentTurtle.Position = StartPoint;
            currentTurtle.CurrentAngle = GetSideAngle();
            currentTurtle.LineLength = LineLength;
            currentTurtle.LineColor = Color.SaddleBrown;
            int sumColorR = -1 * (int)Color.SaddleBrown.R / (int)Iterations;
            int sumColorG = ((int)Color.Green.G - (int)Color.SaddleBrown.G) / (int)Iterations;
            int sumColorB = -1 * (int)Color.SaddleBrown.B / (int)Iterations;

            CurrentPoint nextTurtle = new CurrentPoint();
            Stack<CurrentPoint> pointStack = new Stack<CurrentPoint>();
            List<Tuple<PointF, PointF, int, Color>> lineList = new List<Tuple<PointF, PointF, int, Color>>();
            Random rand = new Random();

            for (int i = 0; i < GrowString.Length; i++)
            {
                switch (GrowString[i])
                {
                    case 'F':
                        {
                            nextTurtle = GetNextPoint(currentTurtle);
                            lineList.Add(new Tuple<PointF, PointF, int, Color>(GetPoint(currentTurtle), GetPoint(nextTurtle),
                                (int)currentTurtle.LineLength / 15, currentTurtle.LineColor));
                            currentTurtle = (CurrentPoint)nextTurtle.Clone();
                            currentTurtle.LineLength *= 0.75;
                            Color newColor = Color.FromArgb((int)currentTurtle.LineColor.R + sumColorR,
                                                            (int)currentTurtle.LineColor.G + sumColorG,
                                                            (int)currentTurtle.LineColor.B + sumColorB);
                            currentTurtle.LineColor = newColor;
                            break;
                        }
                    case '+':
                        {
                            currentTurtle.CurrentAngle += rand.Next((int)Angle / 10, (int)Angle);
                            break;
                        }
                    case '-':
                        {
                            currentTurtle.CurrentAngle -= rand.Next((int)Angle / 10, (int)Angle);
                            break;
                        }
                    case '[':
                        {
                            pointStack.Push((CurrentPoint)currentTurtle.Clone());
                            break;
                        }
                    case ']':
                        {
                            currentTurtle = pointStack.Pop();
                            break;
                        }
                }
            }
            return lineList;
        }

        private void Grow()
        {
            StringBuilder current = new StringBuilder();
            StringBuilder next = new StringBuilder();
            next.Append(Axiom);
            bool found = false;
            int iteration = 0;

            while (iteration < Iterations)
            {
                current.Clear();
                current.Append(next);
                next.Clear();

                for (int i = 0; i < current.Length; i++)
                {
                    found = false;
                    for (int j = 0; j < Rules.Count; j++)
                    {
                        if (current.ToString(i, 1) == Rules[j].rule.ToString())
                        {
                            next.Append(Rules[j].pattern);
                            found = true;
                        }
                    }
                    if (!found)
                        next.Append(current[i]);
                }
                iteration++;
            }
            GrowString = next.ToString();
            //System.Diagnostics.Debug.WriteLine($"GrowString: {GrowString}");
        }

        private double GetSideAngle()
        {
            switch (Side)
            {
                case 'R':
                    return 0;
                case 'D':
                    return 90;
                case 'L':
                    return 180;
                case 'U':
                    return 270;
            }
            return 0;
        }

        public class Rule
        {
            public char rule;
            public string pattern;

            public Rule(string line)
            {
                string[] subStrings = line.Trim().Split(new string[] { "->" }, StringSplitOptions.None);
                rule = subStrings[0].Trim()[0];
                pattern = subStrings[1];
            }
        }

        public class CurrentPoint: ICloneable
        {
            public PointF Position;
            public double LineLength;
            public double CurrentAngle;
            public Color LineColor;

            public object Clone() { return this.MemberwiseClone(); }
        }

        private CurrentPoint GetNextPoint(CurrentPoint point)
        {
            double x = point.Position.X + point.LineLength * Math.Cos(point.CurrentAngle * Math.PI / 180);
            double y = point.Position.Y + point.LineLength * Math.Sin(point.CurrentAngle * Math.PI / 180);

            PointF newPoint = new PointF((float)x, (float)y);
            CurrentPoint res = (CurrentPoint)point.Clone();
            res.Position = newPoint;
            return res;
        }

        private PointF GetPoint(CurrentPoint cp)
        {
            return new PointF(cp.Position.X, cp.Position.Y);
        }
    }
}
