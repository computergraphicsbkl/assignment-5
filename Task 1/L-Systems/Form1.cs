﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LSystems;

namespace L_Systems
{
    public partial class Form1 : Form
    {
        Graphics graphics;
        const string CONFIG_PATH = "../../../config.txt";
        int LINE_LENGTH = 300;
        string AXIOM;
        double ANGLE;
        byte ITERATIONS;
        string RULES;
        char SIDE;
        List<Tuple<PointF, PointF>> lineList;
        List<Tuple<PointF, PointF, int, Color>> treeLineList;

        public Form1()
        {
            InitializeComponent();
            graphics = pictureBox.CreateGraphics();
        }

        private void ReadFile()
        {
            string text = System.IO.File.ReadAllText(CONFIG_PATH);
            Parse(text);
        }

        private void Parse(string text)
        {
            string[] firstLine = text.Split(new string[] { Environment.NewLine }, StringSplitOptions.None)[0].Split();
            AXIOM = firstLine[0];
            ANGLE = Convert.ToDouble(firstLine[1]);
            ITERATIONS = Convert.ToByte(firstLine[2]);
            SIDE = firstLine[3][0];
            int index = text.IndexOf(Environment.NewLine);
            RULES = text.Substring(index + Environment.NewLine.Length);
        }

        private void Draw()
        {
            LSystem lsystem = new LSystem(RULES);
            lsystem.Axiom = AXIOM;
            lsystem.Angle = ANGLE;
            lsystem.Iterations = ITERATIONS;
            lsystem.LineLength = LINE_LENGTH;
            lsystem.Side = SIDE;
            lsystem.StartPoint = new Point(400, 400);

            //lineList = lsystem.GetLines();
            treeLineList = lsystem.GetTreeLines();
            foreach (Tuple<PointF, PointF, int, Color> tp in treeLineList)
            {
                if (tp.Item1.X >= pictureBox.Width || tp.Item1.X <= 0 || tp.Item1.Y >= pictureBox.Height || tp.Item1.Y <= 0
                || tp.Item2.X >= pictureBox.Width || tp.Item2.X <= 0 || tp.Item2.Y >= pictureBox.Height || tp.Item2.Y <= 0)
                {
                    treeLineList = ScaleAutoTree(treeLineList, new Size(pictureBox.Width, pictureBox.Height));
                    break;
                }
            }
            PrintTreeLines();
        }

        private List<Tuple<PointF, PointF>> ScaleAuto(List<Tuple<PointF, PointF>> lines, Size S)
        {
            float xMin = float.MaxValue;
            float xMax = float.MinValue;
            float yMin = float.MaxValue;
            float yMax = float.MinValue;

            foreach (Tuple<PointF, PointF> tuple in lines)
            {
                float tXMax = Math.Max(tuple.Item1.X, tuple.Item2.X);
                float tXMin = Math.Min(tuple.Item1.X, tuple.Item2.X);
                float tYMax = Math.Max(tuple.Item1.Y, tuple.Item2.Y);
                float tYMin = Math.Min(tuple.Item1.Y, tuple.Item2.Y);
                if (tXMin < xMin)
                    xMin = tXMin;
                if (tXMax > xMax)
                    xMax = tXMax;
                if (tYMin < yMin)
                    yMin = tYMin;
                if (tYMax > yMax)
                    yMax = tYMax;
            }

            float x, y, scaling = 0;
            x = (xMax - xMin) / 0.9f;
            y = (yMax - yMin) / 0.9f;

            float tempX = S.Width / x;
            float tempY = S.Height / y;
            if (tempX > tempY)
                scaling = tempY;
            else
                scaling = tempX;

            List<Tuple<PointF, PointF>> res = new List<Tuple<PointF, PointF>>();
            foreach (Tuple<PointF, PointF> line in lines)
            {
                PointF newP1 = new PointF((line.Item1.X - xMin - 10) * scaling + 40,
                    (line.Item1.Y - yMin - 10) * scaling + 40);
                PointF newP2 = new PointF((line.Item2.X - xMin - 10) * scaling + 40,
                     (line.Item2.Y - yMin - 10) * scaling + 40);
                res.Add(new Tuple<PointF, PointF>(newP1, newP2));
            }
            return res;
        }

        private List<Tuple<PointF, PointF, int, Color>> ScaleAutoTree(List<Tuple<PointF, PointF, int, Color>> lines, Size S)
        {
            float xMin = float.MaxValue;
            float xMax = float.MinValue;
            float yMin = float.MaxValue;
            float yMax = float.MinValue;

            foreach (Tuple<PointF, PointF, int, Color> tuple in lines)
            {
                float tXMax = Math.Max(tuple.Item1.X, tuple.Item2.X);
                float tXMin = Math.Min(tuple.Item1.X, tuple.Item2.X);
                float tYMax = Math.Max(tuple.Item1.Y, tuple.Item2.Y);
                float tYMin = Math.Min(tuple.Item1.Y, tuple.Item2.Y);
                if (tXMin < xMin)
                    xMin = tXMin;
                if (tXMax > xMax)
                    xMax = tXMax;
                if (tYMin < yMin)
                    yMin = tYMin;
                if (tYMax > yMax)
                    yMax = tYMax;
            }

            float x, y, scaling = 0;
            x = (xMax - xMin) / 0.9f;
            y = (yMax - yMin) / 0.9f;

            float tempX = S.Width / x;
            float tempY = S.Height / y;
            if (tempX > tempY)
                scaling = tempY;
            else
                scaling = tempX;

            double prevLength = LineLength(lines[0].Item1, lines[0].Item2);
            PointF newP1_test = new PointF((lines[0].Item1.X - xMin - 10) * scaling + 40,
                    (lines[0].Item1.Y - yMin - 10) * scaling + 40);
            PointF newP2_test = new PointF((lines[0].Item2.X - xMin - 10) * scaling + 40,
                 (lines[0].Item2.Y - yMin - 10) * scaling + 40);
            double newLength = LineLength(newP1_test, newP2_test);
            double sizeCoef = newLength / prevLength;

            List<Tuple<PointF, PointF, int, Color>> res = new List<Tuple<PointF, PointF, int, Color>>();
            foreach (Tuple<PointF, PointF, int, Color> line in lines)
            {
                PointF newP1 = new PointF((line.Item1.X - xMin - 10) * scaling + 40,
                    (line.Item1.Y - yMin - 10) * scaling + 40);
                PointF newP2 = new PointF((line.Item2.X - xMin - 10) * scaling + 40,
                     (line.Item2.Y - yMin - 10) * scaling + 40);
                res.Add(new Tuple<PointF, PointF, int, Color>(newP1, newP2, (int)(line.Item3 * sizeCoef), line.Item4));
            }
            return res;
        }

        private double LineLength(PointF p1, PointF p2)
        {
            return Math.Sqrt(Math.Pow(p2.X - p1.X, 2) + Math.Pow(p2.Y - p1.Y, 2));
        }

        private void PrintLines()
        {
            foreach (Tuple<PointF, PointF> tp in lineList)
            {
                //System.Threading.Thread.Sleep(300);
                graphics.DrawLine(Pens.Black, tp.Item1, tp.Item2);
            }
            //graphics.DrawLine(Pens.Black, new Point(0, pictureBox.Height / 2), new Point(pictureBox.Width, pictureBox.Height / 2));
            //graphics.DrawLine(Pens.Black, new Point(pictureBox.Width / 2, 0), new Point(pictureBox.Width / 2, pictureBox.Height));
        }

        private void PrintTreeLines()
        {
            foreach (Tuple<PointF, PointF, int, Color> tp in treeLineList)
            {
                //System.Threading.Thread.Sleep(300);
                Pen pen = new Pen(tp.Item4, tp.Item3);
                pen.StartCap = System.Drawing.Drawing2D.LineCap.Round;
                pen.EndCap = System.Drawing.Drawing2D.LineCap.Round;
                graphics.DrawLine(pen, tp.Item1, tp.Item2);
            }
        }

        private void RefreshButton_Click(object sender, EventArgs e)
        {
            pictureBox.Refresh();
            graphics.ResetTransform();
            ReadFile();
            Draw();
        }

        private void PictureBox_Click(object sender, MouseEventArgs e)
        {
            if (e.Button.Equals(MouseButtons.Left))
            {
                if (treeLineList != null)
                {
                    float scale = float.Parse(textBox.Text);
                    graphics.ScaleTransform(scale, scale);
                    float dx = (pictureBox.Width / 2) - e.Location.X - (pictureBox.Width / 2) * (1 - 1 / scale);
                    float dy = (pictureBox.Height / 2) - e.Location.Y - (pictureBox.Height / 2) * (1 - 1 / scale);
                    graphics.TranslateTransform(dx, dy);
                    pictureBox.Refresh();
                    PrintTreeLines();
                }
            }
            else if (e.Button.Equals(MouseButtons.Right))
            {
                pictureBox.Refresh();
                graphics.ResetTransform();
                ReadFile();
                Draw();
            }
        }
    }
}
