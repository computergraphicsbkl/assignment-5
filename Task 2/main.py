from tkinter import *
from plotly import graph_objs
from plotly.subplots import make_subplots
import numpy
import random


def square(surface, x, y, edge_length, displacement_statement):
    displacement = eval(displacement_statement)
    height_sum = 0
    for dx in [-edge_length // 2, edge_length // 2]:
        for dy in [-edge_length // 2, edge_length // 2]:
            height_sum += surface[x + dx, y + dy]
    height_average = height_sum / 4
    surface[x, y] = height_average + displacement


def diamond(surface, x, y, edge_length, displacement_statement):
    dimension = len(surface)
    displacement = eval(displacement_statement)
    height_sum = 0
    points_count = 0
    for dx in [-edge_length // 2, 0, edge_length // 2]:
        for dy in [-edge_length // 2, 0, edge_length // 2]:
            if (dx == 0 or dy == 0) and not (dx == 0 and dy == 0):
                if 0 <= x + dx < dimension and 0 <= y + dy < dimension:
                    height_sum += surface[x + dx, y + dy]
                    points_count += 1
                elif not 0 <= x + dx < dimension:
                    height_sum -= surface[x - dx, y + dy]
                    points_count -= 1
                else:
                    height_sum -= surface[x + dx, y - dy]
                    points_count -= 1
    height_average = height_sum / points_count
    surface[x, y] = height_average + displacement


def show_surfaces(surfaces, columns_count, height, z_range):
    rows_count = len(surfaces) // columns_count + (1 if len(surfaces) % columns_count != 0 else 0)
    figure = make_subplots(rows=rows_count, cols=columns_count, specs=[[{"is_3d": True}] * columns_count] * rows_count)
    for i in range(len(surfaces)):
        dimension = len(surfaces[i])
        xgrid, ygrid = numpy.meshgrid(range(dimension), range(dimension))
        figure.add_trace(graph_objs.Surface(x=xgrid, y=ygrid, z=surfaces[i]),
                         row=i // columns_count + 1,
                         col=i % columns_count + 1)
        figure.update_layout({"scene" + str(i + 1): dict(zaxis=dict(range=z_range))})
    figure.update_layout(title="Diamond square", height=rows_count*height)
    figure.show()


def generate_render_button_click():
    global surfaces
    try:
        dimension_exponent = int(dimension_exponent_text.get())
        bottom_left = float(bottom_left_text.get())
        bottom_right = float(bottom_right_text.get())
        top_left = float(top_left_text.get())
        top_right = float(top_right_text.get())
        displacement_statement = displacement_text.get()
        show_all = show_all_int.get() == 1
        columns_count = int(columns_count_text.get())
        height = int(height_text.get())
        z_range = eval(z_range_text.get())
    except ValueError:
        return
    surfaces = []
    dimension = 2**dimension_exponent + 1
    surface = numpy.zeros((dimension, dimension))
    surface[0, 0] = bottom_left
    surface[dimension - 1, 0] = bottom_right
    surface[0, dimension - 1] = top_left
    surface[dimension - 1, dimension - 1] = top_right
    edge_length = dimension - 1
    if show_all:
        surfaces.append(surface[::edge_length, ::edge_length].copy())
    while edge_length > 1:
        # Square step
        for x in range(edge_length // 2, dimension - 1, edge_length):
            for y in range(edge_length // 2, dimension - 1, edge_length):
                square(surface, x, y, edge_length, displacement_statement)
        # Diamond step
        for x in range(edge_length // 2, dimension - 1, edge_length):
            for y in range(edge_length // 2, dimension - 1, edge_length):
                diamond(surface, x - edge_length //2, y, edge_length, displacement_statement)
                diamond(surface, x, y - edge_length // 2, edge_length, displacement_statement)
        for x in range(edge_length // 2, dimension - 1, edge_length):
            diamond(surface, x, dimension - 1, edge_length, displacement_statement)
        for y in range(edge_length // 2, dimension - 1, edge_length):
            diamond(surface, dimension - 1, y, edge_length, displacement_statement)
        if show_all or edge_length == 2:
            surfaces.append(surface[::(edge_length // 2), ::(edge_length // 2)].copy())
        edge_length = edge_length // 2
    show_surfaces(surfaces, columns_count, height, z_range)


def render_button_click():
    try:
        columns_count = int(columns_count_text.get())
        height = int(height_text.get())
        z_range = eval(z_range_text.get())
    except ValueError:
        return
    except SyntaxError:
        return
    try:
        show_surfaces(surfaces, columns_count, height, z_range)
    except NameError:
        return


root = Tk()
root.title("Diamond square")
root.geometry("400x480")
root.resizable(False, False)

dimension_exponent_text = StringVar()
dimension_exponent_text.set("5")
bottom_left_text = StringVar()
bottom_left_text.set("0")
bottom_right_text = StringVar()
bottom_right_text.set("0")
top_left_text = StringVar()
top_left_text.set("0")
top_right_text = StringVar()
top_right_text.set("0")
displacement_text = StringVar()
displacement_text.set("random.uniform(-edge_length**0.8, edge_length)")
show_all_int = IntVar()
show_all_int.set(1)
columns_count_text = StringVar()
columns_count_text.set("3")
height_text = StringVar()
height_text.set("800")
z_range_text = StringVar()
z_range_text.set("[-20, 40]")

Label(text="Dimension exponent").pack(fill=BOTH)
Entry(textvariable=dimension_exponent_text).pack(fill=BOTH)
Label(text="Bottom left").pack(fill=BOTH)
Entry(textvariable=bottom_left_text).pack(fill=BOTH)
Label(text="Bottom right").pack(fill=BOTH)
Entry(textvariable=bottom_right_text).pack(fill=BOTH)
Label(text="Top left").pack(fill=BOTH)
Entry(textvariable=top_left_text).pack(fill=BOTH)
Label(text="Top right").pack(fill=BOTH)
Entry(textvariable=top_right_text).pack(fill=BOTH)
Label(text="Displacement statement (variables: x, y, edge_length)").pack(fill=BOTH)
Entry(textvariable=displacement_text).pack(fill=BOTH)
Checkbutton(text="Show all levels", variable=show_all_int).pack(fill=BOTH)
Label(text="Columns count").pack(fill=BOTH)
Entry(textvariable=columns_count_text).pack(fill=BOTH)
Label(text="Graph height").pack(fill=BOTH)
Entry(textvariable=height_text).pack(fill=BOTH)
Label(text="Z range ([number, number] or None)").pack(fill=BOTH)
Entry(textvariable=z_range_text).pack(fill=BOTH)
Button(text="Generate and render", command=generate_render_button_click).pack(fill=BOTH)
Button(text="Render", command=render_button_click).pack(fill=BOTH)

root.mainloop()